function plotAlgorithmTrace(trace,Metric, Instance)

    plot (trace(:,1), trace(:,2),'-o','LineWidth',1,'MarkerSize',3,'MarkerFaceColor','g');

    metricName=getMetricName(Metric);
    xlabel('Generations'); ylabel(metricName);
    title(['GA4MSA solving ' ,Instance,' with best Score ', num2str(max(trace(:,2)))]);

    %set(gcf,'PaperPositionMode','auto')
    saveas(gcf,['GA4MSA_',Instance,'.jpg'],'jpg');
        
    clf();

end
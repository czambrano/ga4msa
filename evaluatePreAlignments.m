function scores= evaluatePreAlignments(Path,Group,Instance)
scores=zeros(1,24);
PathInstance=strcat(Path,'\',Group,'\',Instance);
namefile={(strcat(PathInstance,'.fasta_aln')),
            (strcat(PathInstance,'.tfa_clu')),
            (strcat(PathInstance,'.tfa_fsa')),
            (strcat(PathInstance,'.tfa_mafft')),
            (strcat(PathInstance,'.tfa_muscle')),
            (strcat(PathInstance,'.tfa_probcons')),
            (strcat(PathInstance,'.tfa_retalign')),
            (strcat(PathInstance,'.tfa_kalign'))};

    for i=0:7
        filename=char(namefile(i+1));
        if exist(filename, 'file') == 2
            [~,MSA]=fastaread(filename);

            MSAstr=convertMSAfromCellToString(MSA,length(MSA));
            scores(i*3+1)=Evaluate(1,MSAstr,length(MSA));
            scores(i*3+2)=Evaluate(2,MSAstr,length(MSA));
            scores(i*3+3)=Evaluate(3,MSAstr,length(MSA));
            
        end
    end
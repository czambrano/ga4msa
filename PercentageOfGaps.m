function P=PercentageOfGaps(MSA)
    P = sum(sum(MSA=='-'))/(size(MSA,1)*size(MSA,2))*100;
end
function pop=RandomInitialization(PopSize,seqs, k)
    pop=cell(PopSize,k);
    MaxSize = MaxSizeUnalignedSeq(seqs);
    for j=1:PopSize
         MSA=generateRandomMSA(seqs,MaxSize,k);
         pop(j,:) = MSA;  
    end
end
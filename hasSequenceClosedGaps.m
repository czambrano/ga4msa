function hasClosedGaps= hasSequenceClosedGaps(seq)
    hasClosedGaps = ~isempty(find(seq=='-'));
end
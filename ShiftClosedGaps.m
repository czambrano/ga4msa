function msa= ShiftClosedGaps(Pm,msa)
 
if(rand()<=Pm)
    while true
        numseq=randi(size(msa,1));
        seq = msa(numseq,:);
        if hasSequenceClosedGaps(seq)
            break;
        end
    end
       
    pos_gaps = find(seq=='-');

    inits = pos_gaps(diff([-1 pos_gaps])>1); 
    try
        pos_init = randi(length(inits));
    catch
        pos_init =0
    end
    
    ini = inits(pos_init );

    if(pos_init ~= length(inits))
             fin = pos_gaps(find(pos_gaps==inits(pos_init+1))-1);
    else
             fin = pos_gaps(length(pos_gaps));
    end

    %[ini fin];
    seq2=[];
    if ini>1
        seq2= seq(1:ini-1);
    end

    if fin<length(seq)
        seq2=[seq2 seq(fin+1:length(seq))];
    end

    newpos=randi(length(seq2));
    NumGaps=fin-ini+1;
    for i=1:NumGaps
        seq2=seqinsertgaps(seq2,newpos,1);
    end
    msa(numseq,:) = seq2;
    msa=DelColsOnlyWithGaps(msa);
end
end


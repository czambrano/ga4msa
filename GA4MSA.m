function [MSABestIndividual, score, headers, res]=GA4MSA(Instance, PopSize, numberofGenerations,Px, Pm, Pr, Metric, Pi, numberOfIterImprove, typeOfIni, Path)

[headers,seqs]=fastaread(strcat('C:\msa\bb3_release\RV11\',Instance,'.tfa'));
k=length(seqs);

res=zeros(numberofGenerations,2);
pop=Initialization(typeOfIni,PopSize,seqs, k, Path, Instance);
fitness=InitialEvaluation(pop,k,PopSize, Metric);

for Eval=1:numberofGenerations
      [Offspring,fitnessoff]=Reproduction(pop,fitness,PopSize,k, Px, Pm, Metric, Pi, numberOfIterImprove);
      [pop,fitness] =        Replacement(Pr, pop, fitness, Offspring,fitnessoff);
      res(Eval,1)=Eval;     
      res(Eval,2)=max(fitness);
end

[MSABestIndividual,score]= BestSelection(fitness,pop);
end
function [newMSA,newscore]=Improvement(MSA, score, k, Metric, Pi, numberOfIterImprove)

if(rand()<=Pi)
    numberOfIterations=0;
    while (numberOfIterations<=numberOfIterImprove)
        newMSA=ShiftClosedGaps(1.0,MSA);
        newscore=Evaluate(Metric,newMSA,k);
        if (newscore>score)
            MSA=newMSA;
            score=newscore;
        end
        numberOfIterations=numberOfIterations+1;
    end 

end
newMSA=MSA;
newscore=score;
     
end
function MSA=DelColsOnlyWithGaps(MSA)
    Cols= sum(MSA~='-')==0;
    MSA=MSA(:, ~Cols);
end
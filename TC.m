function P=TC(MSA)
    NumberOFColumns= size(MSA,2);
    P=sum(all(MSA==repmat(MSA(1,:),size(MSA,1),1)));
    P = (P/NumberOFColumns)*100;
end
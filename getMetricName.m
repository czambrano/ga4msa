function metricName=getMetricName(Metric)

switch Metric
   case 1
         metricName='Sum-of-Pairs';
   case 2
         metricName='% Totally Aligned Columns';
   case 3
        metricName='% Non-Gaps';
end

end
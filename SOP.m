function SumOfPairs=SOP(MSAstr,NumberOfSequences)
    Matriz=pam(250);
    SumOfPairs=0;

    Longitud = size(MSAstr(1,:),2);
    
    for i=1:NumberOfSequences-1
        for j=i+1:NumberOfSequences
            for k=1:Longitud
                SumOfPairs=SumOfPairs+getDistance(Matriz,MSAstr(i,k),MSAstr(j,k));
            end 
        end
    end
end
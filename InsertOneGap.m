function MSAMut=InsertOneGap(MSA)
    for i=1:size(MSA,1)
        MSAMut(i,:)= seqinsertgaps(MSA(i,:),randi(size(MSA,2)),1);
    end
     MSAMut=DelColsOnlyWithGaps(MSAMut)
end 
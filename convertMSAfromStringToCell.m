function MSA=convertMSAfromStringToCell(MSAStr,k)
    MSA=cell(1,k);
    for i=1:k
          MSA(1,i) = cellstr(MSAStr(i,:));
    end
   
end
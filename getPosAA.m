function Position=getPosAA(AA)
aminoacids=['A','R','N','D','C','Q','E','G','H','I','L','K','M','F','P','S','T','W','Y','V','B','Z','X','-'];
pos1= find(aminoacids==AA);
 if ~isempty(pos1)
     Position=pos1(1);
 else
     Position=24;
 end
end
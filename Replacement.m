function [NewPop, newfitness] = Replacement(Pr, pop, fitness, offspring, fitnessoff)
     NumIndSons= length(fitness)*Pr;
     NumIndParents= length(fitness)-NumIndSons;
    
    [fitnessordered, pos1] = sort(fitness,'descend');
    [fitnessoffordered, pos2] = sort(fitnessoff,'descend');
 
    NewPop = [pop(pos1(1:NumIndParents),:);offspring(pos2(1:NumIndSons),:)];
    newfitness = [fitnessordered(1:NumIndParents),fitnessoffordered(1:NumIndSons)];
    
end
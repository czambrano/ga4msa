function MaxSize = MaxSizeUnalignedSeq(seqs)
    MaxSize=0;
    for i=1:length(seqs)
        seq=char(seqs(i));
        if length(seq) > MaxSize
            MaxSize=length(seq);
        end
    end
end
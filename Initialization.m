function pop=Initialization(Type,PopSize,seqs, k, Path, Instance)
 if(Type==1)
    pop=RandomInitialization(PopSize,seqs, k);
 else
    pop=InitializationwithPreAlignments(Path, Instance, PopSize, k);
 end

end
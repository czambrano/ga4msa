function fitness=InitialEvaluation(pop,k, PopSize, Metric)
    for i=1:PopSize
       MSAstr=convertMSAfromCellToString(pop(i,:),k);
       fitness(i)=Evaluate(Metric,MSAstr,k);
    end
    
end

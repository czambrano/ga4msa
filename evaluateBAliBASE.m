clc; clear;
Group='20';
Limit=41; %38 44 41 30 40 15
Path='C:\msa\aligned';

resultados=zeros(Limit,24);
for i=1:Limit
    Instance=strcat('BB',Group,'0');
    if i<10
        Instance=strcat(Instance,'0');
    end
    Instance=strcat(Instance,num2str(i))

    resultados(i,:)=evaluatePreAlignments(Path,strcat('RV',Group),Instance);
end
function score=Evaluate(Metric,MSAstr,NumberOfSequences)

switch Metric
   case 1
         score=SOP(MSAstr,NumberOfSequences);
   case 2
         score=TC(MSAstr);
   case 3
        score=NonGaps(MSAstr);
  otherwise
     score=SOP(MSAstr,NumberOfSequences);
end

end
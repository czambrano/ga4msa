function P=NonGaps(MSA)

    NumberOFCaracters= length(MSA(1,:))*size(MSA,1);
    P = 100-(sum(sum(MSA=='-'))/NumberOFCaracters*100);

end
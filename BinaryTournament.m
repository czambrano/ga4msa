function MSASeleccionado = BinaryTournament(pop,fitness,popSize)

    pos1=randi(popSize);
    pos2=randi(popSize);

    Score1=fitness(pos1);
    Score2=fitness(pos2);
    
    if(Score1>Score2)
        MSASeleccionado = pop(pos1,:); 
    else
        MSASeleccionado = pop(pos2,:); 
    end


end
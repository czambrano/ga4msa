function pop=InitializationwithPreAlignments(Path, Instance, PopSize, k)
PathInstance=strcat(Path,Instance);
namefile={(strcat(PathInstance,'.fasta_aln')),
            (strcat(PathInstance,'.tfa_clu')),
            (strcat(PathInstance,'.tfa_fsa')),
            (strcat(PathInstance,'.tfa_mafft')),
            (strcat(PathInstance,'.tfa_muscle')),
            (strcat(PathInstance,'.tfa_probcons')),
            (strcat(PathInstance,'.tfa_retalign')),
            (strcat(PathInstance,'.tfa_kalign'))};

    pop=cell(PopSize,k); numberOfIndividuals=0;
    for i=1:size(namefile,1)    
        filename=char(namefile(i));
        if exist(filename, 'file') == 2
            [~,pop(i,:)]=fastaread(filename);
            numberOfIndividuals=numberOfIndividuals+1;
        end
    end

    if(numberOfIndividuals<2) 
        exit;
    end

    while numberOfIndividuals<=PopSize     
        pos1=randi(numberOfIndividuals);
        pos2=randi(numberOfIndividuals);

        MSA1=pop(pos1,:);
        MSA2=pop(pos2,:);

        MSAstr1=convertMSAfromCellToString(MSA1,k);
        MSAstr2=convertMSAfromCellToString(MSA2,k);
        
        [MSAHijo1 , MSAHijo2] =SinglePointCrossover(1,MSAstr1, MSAstr2,k);
        pop(numberOfIndividuals+1,:)=convertMSAfromStringToCell(MSAHijo1,k);
        pop(numberOfIndividuals+2,:)=convertMSAfromStringToCell(MSAHijo2,k);
       
        numberOfIndividuals=numberOfIndividuals+2;
    end

end

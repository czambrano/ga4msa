function MSA= generateRandomMSA(seqs,MaxSize,k)
    MSA=cell(1,k);

    for i=1:k
          seq=char(seqs(i));
          seq=seqinsertgaps(seq,randi(length(seq),1,MaxSize-length(seq)),1);
          
          pos=randi(length(seq)-4);
          seq=seqinsertgaps(seq,[pos pos+1 pos+2],1);
          
          MSA(1,i)=cellstr(seq);
    end
end



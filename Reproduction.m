function [Offspring,fitnessoff]=Reproduction(pop,fitness, PopSize, k, Px, Pm, Metric, Pi, numberOfIterImprove)

    Offspring=cell(PopSize,k);
    fitnessoff=zeros(1,PopSize);
    for x=1:2:PopSize
        
        MSA1=BinaryTournament(pop,fitness,PopSize);
        MSA2=BinaryTournament(pop,fitness,PopSize);
        MSAstr1=convertMSAfromCellToString(MSA1,k);
        MSAstr2=convertMSAfromCellToString(MSA2,k);
        
        [MSAHijo1 , MSAHijo2] =SinglePointCrossover(Px,MSAstr1, MSAstr2,k);
        MSAHijo1 = ShiftClosedGaps(Pm,MSAHijo1);
        MSAHijo2 = ShiftClosedGaps(Pm,MSAHijo2);
        
        score1=Evaluate(Metric,MSAHijo1,k);
        [MSAHijo1,score1]=Improvement(MSAHijo1, score1,k,Metric, Pi, numberOfIterImprove);
        fitnessoff(x)= score1;
        
        score2=Evaluate(Metric,MSAHijo2,k);
        [MSAHijo2,score2]=Improvement(MSAHijo2, score2,k,Metric, Pi, numberOfIterImprove);
        fitnessoff(x+1)=score2;
        
        Offspring(x,:)=convertMSAfromStringToCell(MSAHijo1,k);
        Offspring(x+1,:)=convertMSAfromStringToCell(MSAHijo2,k);
    end
    
end
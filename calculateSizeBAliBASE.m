clc; clear;
Groups=[11,12,20,30,40,50];
Limits=[38,44,41,30,40,15];
Path='C:\msa\bb3_release';

resultados=zeros(208,2);
k=1;
for j=6:6
    Limit=Limits(j);
    Group=Groups(j);
    for i=1:Limit
        Instance=strcat('BB',num2str(Group),'0');
        if i<10
            Instance=strcat(Instance,'0');
        end
        Instance=strcat(Instance,num2str(i))
      

        filename=strcat(Path,'\RV', num2str(Group),'\',Instance,'.tfa');
        [~,seqs]=fastaread(filename);
        resultados(k,1)= size(seqs,2);
        resultados(k,2)= MaxSizeUnalignedSeq(seqs);
        k=k+1;
    end
end
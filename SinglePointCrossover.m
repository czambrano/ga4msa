function [MSAhijo1, MSAhijo2] = SinglePointCrossover(Px, msa1, msa2,k)

if(rand()<=Px)
    c1=size(msa1,2); c2=size(msa2,2); 

    %Generar una posicion aleatoria de corte recto
    CutPosition = randi([2,c1-1],1,1);

    longitudes=[];
    bloqueizquierdo=[];
    bloquederecho=[];

    for i=1:k %Para recorrer secuencia x secuencia en cada padre
       seq1=msa1(i,:); %secuencia (i) del primer padre
       seq2=msa2(i,:); %secuencia (i) del segundo padre

       %Corte del primer bloque
       tempseq1=seq1(1,1:CutPosition); %l�nea (i) del Bloque Izquierdo
       countempseq1 = sum(sum(tempseq1~='-')); %Cu�ntos no gaps hay en esta l�nea??

       %Cu�ntos elementos de la seq 2 debo ignorar para unir en bloques?
       posini = find(cumsum(seq2(1,:)~='-')==countempseq1,1)+1;
       if isempty(posini)
           posini=1;
       end
       tempseq2 = seq2(1,posini:end); %Extraigo el bloque derecho;

       %Guardo las mediciones de longitud y punto inicial de extraccion
       longitudes = [longitudes;length(tempseq2) posini];

       bloqueizquierdo= [bloqueizquierdo; tempseq1]; %Colecci�n de cortes del padre 1 alineados

       % �Para qu� guardar la longitud? --> para saber qu� longitud debe tener
       % cada secuencia en el bloque derecho,si no tienen la misma longitud, se
       % debe rellenar con gaps para emparejarlos
    end


    % A continuaci�n extraigo y formo una nueva matriz con los gaps
       % insertados de relleno de la segunda matriz
       longmax = max(longitudes(:,1)); %Indica qu� longitud debe tener a lo ancho el bloque derecho

       for i=1:k
           seq2=msa2(i,:);
           tempseq2 = seq2(1,longitudes(i,2):end); %Extraigo una secuencia del bloque derecho
           r = longmax - longitudes(i,1); %Cu�ntos gaps debo agregar en esta l�nea? Se agregan o no?

           for j=1:r
             tempseq2 = strcat('-', tempseq2); %Se agrega gaps para emparejar, si es necesario
           end       

           bloquederecho = [bloquederecho; tempseq2]; %Colecci�n de cortes del padre 2 alineados
       end


    %Finalmente, formar el primer hijo!!
    MSAhijo1 = [bloqueizquierdo bloquederecho];

    % Formaci�n del Hijo 2
    bloqueizquierdo = [];
    bloquederecho = [];
    longitudes = [c2-longitudes(:,1) longitudes(:,2)-1];
    longmax = max(longitudes(:,1));

    for i=1:k
           seq2=msa2(i,:);
           tempseq1 = seq2(1,1:longitudes(i,2)); %Extraigo una secuencia del bloque izquierdo
           r = longmax - longitudes(i,1); %Cu�ntos gaps debo agregar en esta l�nea? Se agregan o no?

           for j=1: r
             tempseq1 = strcat(tempseq1,'-'); %Se agrega gaps para emparejar, si es necesario
           end       

           bloqueizquierdo = [bloqueizquierdo; tempseq1]; %Colecci�n de cortes del padre 2 alineados
    end

    bloquederecho = msa1(:,CutPosition+1:end);
    MSAhijo2 = [bloqueizquierdo bloquederecho];
    
    MSAhijo1=DelColsOnlyWithGaps(MSAhijo1);
    MSAhijo2=DelColsOnlyWithGaps(MSAhijo2);
    
else
   MSAhijo1=msa1;  MSAhijo2=msa2;
end

end
clc; clear;
%%%%%%%%%%%Configuration of the Algorithm%%%%%%%%%%%%%%%%%%%
%Problem Name
Instance='BB11001';

%Parameters of the Algorithm
PopSize=20; 
Px=0.8; Pm=0.2; numberofGenerations=100;
Metric=1; %1=SOP; 2=TC 3=NonGaps

%Local Search
Pi=0.5; 
numberOfIterImprove=10;

%Percentage of Replacement
Pr=0.5;

%Initialiation Type
TypeOfIni=2; %1=Random 2=PreAlignments
PathOfTheAligments='C:\msa\aligned\RV11\'; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[MSABestIndividual, score, headers,trace]=GA4MSA(Instance,PopSize, numberofGenerations, Px, Pm, Pr, Metric, Pi, numberOfIterImprove, TypeOfIni, PathOfTheAligments)

%%%%%%%%%%%%%%%%% Write MSA in a File with Fasta Format%%%%%%%%%%%%
fastawrite(strcat('GA4MSA.fasta'),headers,MSABestIndividual);
seqalignviewer('GA4MSA.fasta');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%Plot  trace of the Algorithm%%%%%%%%%%%%%%%%5
plotAlgorithmTrace(trace,Metric, Instance);



